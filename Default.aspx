﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <asp:LoginView ID="LoginView1" runat="server" ViewStateMode="Disabled">
        <AnonymousTemplate>
             <div class="jumbotron">
                <h2>Please log in...</h2>  
            </div>
        </AnonymousTemplate>
        <LoggedInTemplate>
        </LoggedInTemplate>
    </asp:LoginView>

</asp:Content>
