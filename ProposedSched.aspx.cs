﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using MHSglobalScheduling;
using System.Diagnostics;

public partial class ProposedSched : System.Web.UI.Page
{
	/*************************************************************************************************************************************/
	// the commented code has not been tested. it has been placed here as a starting point to save the proposed schedule to the database.
	//  TO DO :
	//			add button to accept changes
	//			uncomment piece by piece to test
	//			write "save" code (most likely for or foreach to write dt as sched)
	/*************************************************************************************************************************************/
	protected void Page_Load(object sender, EventArgs e)
	{
		try
		{
			DataTable dt = new DataTable();
			DataTable sch = new DataTable();
			sch.Clear();
			sch.Columns.Add("MFGCELL");
			sch.Columns.Add("Capacity");
			sch.Columns.Add("BUCKET_START_TIME");
			sch.Columns.Add("PRODHRS");
			var frmDt = Request.QueryString["fromDate"].ToString() ?? string.Empty;
			int frmYr = Convert.ToInt32(frmDt.Substring(frmDt.LastIndexOf("/") + 1));
			int frmMnth = Convert.ToInt32(frmDt.Substring(0, 2));
			int frmDay = Convert.ToInt32(frmDt.Substring(frmDt.IndexOf("/") + 1, 2));
			int buckets = Convert.ToInt32(Request.QueryString["buckets"].ToString() ?? "0");
			DateTime fromDate = new DateTime(frmYr, frmMnth, frmDay);
			var cell = string.Empty;
			decimal prodhrsum = 0M;

			using (var cmd = new SqlCommand("WO.WORKORDER_select_forReschedule", new SqlConnection(WebConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)) { CommandType = CommandType.StoredProcedure })
			{
				cmd.Parameters.AddWithValue("@MFG_TYPE", Request.QueryString["MFG_TYPE"] ?? string.Empty);
				cmd.Parameters.AddWithValue("@fromDate", fromDate.ToString("MM/dd/yyyy"));
				cmd.Parameters.AddWithValue("@buckets", buckets);

				using (var da = new SqlDataAdapter(cmd)) { da.Fill(dt); }
			}

			if (dt.Rows.Count > 0)
			{
				cell = dt.Rows[0]["MFGCELL"].ToString();
				DateTime date = fromDate;

				for (int i = 0; i < dt.Rows.Count; i++)
				{ // iterate through dt
					dt.Rows[i]["BUCKET_START_TIME"] = date.ToString("yyyy-MM-dd");

					if (cell == dt.Rows[i]["MFGCELL"].ToString())
					{
						if (Convert.ToDecimal(dt.Rows[i]["Capacity"]) > (prodhrsum + Convert.ToDecimal(dt.Rows[i]["PRODHRS"]))) { prodhrsum += Convert.ToDecimal(dt.Rows[i]["PRODHRS"]); }
						else
						{
							prodhrsum = (prodhrsum + Convert.ToDecimal(dt.Rows[i]["PRODHRS"])) - Convert.ToDecimal(dt.Rows[i]["Capacity"]);
							date = date.AddDays(1);
						}
					}
					else
					{
						prodhrsum = 0;
						date = fromDate;
						cell = dt.Rows[i]["MFGCELL"].ToString();
					}
				}

				var adjustedSched = dt.AsEnumerable().ToPivotTable(
														item => string.Format("{0: MMM dd}", item.Field<DateTime>("BUCKET_START_TIME")),
														item => item.Field<string>("MFGCELL"),
														//item => item.Field<double>("Capacity"),
														items => items.Any() ? string.Format("{0: 0.00}", items.Sum(x => x.Field<decimal>("PRODHRS"))) : "0").AsDataView();

				Session.Add("propSched", dt);

				gvWorkCenter.DataSource = adjustedSched;
			}

			gvWorkCenter.DataBind();
		}
		catch (Exception ex)
		{
			Debug.WriteLine(ex.Message.ToString());
			gvWorkCenter.DataBind();
		}
	}
	protected void gvWorkCenter_RowDataBound(object sender, GridViewRowEventArgs e)
	{// uncomment to add links so user can see related WOs for proposed schedule day (need to add proposedWOs.aspx with working datasource for this to work)
	 //if (e.Row.RowType == DataControlRowType.DataRow)
	 //{
	 //	for (int i = 1; i < e.Row.Cells.Count; i++)
	 //	{
	 //		var hl = new HyperLink() { CssClass = "open_ajax", Text = string.Format(e.Row.Cells[i].Text), NavigateUrl = $"~/proposedWOs.aspx?cell={e.Row.Cells[0].Text}&date={gvWorkCenter.HeaderRow.Cells[i].Text}" };
	 //		e.Row.Cells[i].Controls.Add(hl);
	 //	}
	 //}
	}

	DataTable Pivot(DataTable dt, DataColumn pivotColumn, DataColumn pivotValue)
	{  // secondary method used to pivot data so dates become headers
	   // find primary key columns 
	   //(i.e. everything but pivot column and pivot value)
		DataTable temp = dt.Copy();
		temp.Columns.Remove(pivotColumn.ColumnName);
		temp.Columns.Remove(pivotValue.ColumnName);
		string[] pkColumnNames = temp.Columns.Cast<DataColumn>()
			.Select(c => c.ColumnName)
			.ToArray();

		// prep results table
		DataTable result = temp.DefaultView.ToTable(true, pkColumnNames).Copy();
		result.PrimaryKey = result.Columns.Cast<DataColumn>().ToArray();
		dt.AsEnumerable()
			.Select(r => r[pivotColumn.ColumnName].ToString())
			.Distinct().ToList()
			.ForEach(c => result.Columns.Add(c, pivotValue.DataType));
		//.ForEach(c => result.Columns.Add(c, pivotColumn.DataType));

		// load it
		foreach (DataRow row in dt.Rows)
		{
			// find row to update
			DataRow aggRow = result.Rows.Find(
				pkColumnNames
					.Select(c => row[c])
					.ToArray());
			// the aggregate used here is LATEST 
			// adjust the next line if you want (SUM, MAX, etc...)

			aggRow[row[pivotColumn.ColumnName].ToString()] = row[pivotValue.ColumnName];

			//for (int i = pkColumnNames.Length - 1; i < result.Columns.Count; i++)
			//{
			//	if (string.IsNullOrEmpty(aggRow[i].ToString()))
			//	{
			//		aggRow[i] = -1;
			//	}
			//}
		}

		return result;
	}

}
