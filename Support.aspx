﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Support.aspx.cs" Inherits="Support" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
        .centerHeaderText th {
            text-align: center;
        }

        .hide {
            visibility: hidden;
        }
        .my-class {
            padding-top: 30px;
            padding-bottom: 30px;
            margin-bottom: 30px;
            color: inherit;
            background-color: #eee;
            border-radius: 6px;
        }
    </style>
            <h2><b>Support</b></h2>
            <div>
                <p class="container my-class">
                    <b><u>Emergency Support Contact:</u></b> Mathew Bodner:<br /> <b><u>Cell:</u></b> (315) 246-2153<br /> <b><u>Email:</u></b> <a href="mailto:mathew.bodner@mhsglobal.com?subject=Bolt-On Application Support">mathew.bodner@mhsglobal.com</a><br /><br />
                </p>
            </div>
            
                
            

</asp:Content>
