﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using MHSglobalScheduling;

public partial class Schedule : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e) { }

	protected void ds_Load(object sender, EventArgs e) { ((SqlDataSource)sender).Select(DataSourceSelectArguments.Empty); }

    protected void btnLoadData_Click(object sender, EventArgs e)
	{
		dsWorkCenter.Select(DataSourceSelectArguments.Empty);
		aNewSched.HRef = String.Format("~/ProposedSched.aspx?MFG_TYPE={0}&fromDate={1}&buckets={2}", ddlCell.SelectedValue, txtDate.Value, ddlBuckets.SelectedValue);
		//aNewSched.Visible = true;																																  
	}

	protected void dsWorkCenter_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
	{
		e.Command.Parameters["@MFG_TYPE"].Value = (ddlCell == null) ? "Select" : ddlCell.SelectedValue;
		e.Command.Parameters["@fromDate"].Value = txtDate.Value;
		e.Command.Parameters["@buckets"].Value = ddlBuckets.SelectedValue;
	}

    protected void gvWorkCenter_RowDataBound(object sender, GridViewRowEventArgs e)
	{
		switch (e.Row.RowType)
		{
			case DataControlRowType.Header:
				for (int i = 1; i < e.Row.Cells.Count; i++)   //except first column
				{
					LinkButton lb = (LinkButton)e.Row.Cells[i].Controls[0];
                    Label lbl = new Label(){Text = lb.Text};
                    e.Row.Cells[i].Controls.Clear();
					e.Row.Cells[i].Controls.Add(lbl);
                }
                //e.Row.CssClass = "centerHeaderText";
                break;
			case DataControlRowType.DataRow:
				for (int i = 0; i < e.Row.Cells.Count; i++)
					if (i > ((e.Row.Cells.Count - 1) - Convert.ToInt32(ddlBuckets.SelectedValue)) && e.Row.Cells[i].Text != string.Empty)
					{
                    var dateValue = Convert.ToDateTime(txtDate.Value);
                    var dateValueNext = dateValue.AddDays(i-3);
                    var dateValueNextStr = (Convert.ToDateTime(dateValueNext).ToString("MM/dd/yyyy"));
                    var capacity = Convert.ToDecimal(e.Row.Cells[2].Text);
                    decimal prodhrs = 00.00m;
                    if (e.Row.Cells[i].Text != "&nbsp;") {
                            prodhrs = Convert.ToDecimal(e.Row.Cells[i].Text);
                        }
                    var hl = new HyperLink() {
							 CssClass = "open_ajax"
							,Text = string.Format(e.Row.Cells[i].Text)
							,NavigateUrl = $"~/WorkOrders.aspx?cell={e.Row.Cells[0].Text}&date={dateValueNextStr}" };
                            
                        e.Row.Cells[i].Controls.Add(hl);
                        if (prodhrs > capacity) { hl.ControlStyle.ForeColor = System.Drawing.Color.Red; }

                    }
                
                break;
			default:
				break;
		}
    }

	protected void gvWorkCenter_PreRender(object sender, EventArgs e)
	{
		var isDate = new DateTime();
		for (int i = 1; ((GridView)sender).HeaderRow != null && i < ((GridView)sender).HeaderRow.Cells.Count; i++)
			if (DateTime.TryParse(gvWorkCenter.HeaderRow.Cells[i].Text, out isDate)) { gvWorkCenter.HeaderRow.Cells[i].Text = gvWorkCenter.HeaderRow.Cells[i].Text.Substring(0, 6); }
    }

	DataTable Pivot(DataTable dt, DataColumn pivotColumn, DataColumn pivotValue)
	{
		// find primary key columns 
		//(i.e. everything but pivot column and pivot value)
		DataTable temp = dt.Copy();
		temp.Columns.Remove(pivotColumn.ColumnName);
		temp.Columns.Remove(pivotValue.ColumnName);
		string[] pkColumnNames = temp.Columns.Cast<DataColumn>()
			.Select(c => c.ColumnName)
			.ToArray();

		// prep results table
		DataTable result = temp.DefaultView.ToTable(true, pkColumnNames).Copy();
		result.PrimaryKey = result.Columns.Cast<DataColumn>().ToArray();
		dt.AsEnumerable()
			.Select(r => r[pivotColumn.ColumnName].ToString())
			.Distinct().ToList()
			.ForEach(c => result.Columns.Add(c, pivotValue.DataType));
		//.ForEach(c => result.Columns.Add(c, pivotColumn.DataType));

		// load it
		foreach (DataRow row in dt.Rows)
		{
			// find row to update
			DataRow aggRow = result.Rows.Find(
				pkColumnNames
					.Select(c => row[c])
					.ToArray());
			// the aggregate used here is LATEST 
			// adjust the next line if you want (SUM, MAX, etc...)

			aggRow[row[pivotColumn.ColumnName].ToString()] = row[pivotValue.ColumnName];

			//for (int i = pkColumnNames.Length - 1; i < result.Columns.Count; i++)
			//{
			//	if (string.IsNullOrEmpty(aggRow[i].ToString()))
			//	{
			//		aggRow[i] = -1;
			//	}
			//}
		}

		return result;
	}


}