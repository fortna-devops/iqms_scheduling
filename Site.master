﻿<%@ Master Language="C#" AutoEventWireup="true" CodeFile="Site.master.cs" Inherits="Site" %>

<!DOCTYPE html>

<html lang="en">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>MHS Global - Scheduler</title>
    <asp:PlaceHolder runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>
    <webopt:bundlereference runat="server" path="~/Content/css" />
    <link href="~/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <style>
        a {
            color: black;
            position: relative;
            text-decoration: none;
        }

        a:hover {
          text-decoration: none;
          background-color: #909090;
          color: white;
          cursor: pointer;
        }

        a:active {
          box-shadow: none;
          top: 5px;
        }
        a.button{
         background-color:#909090;
         border:1px solid #000000;
         border-radius:5px;
         color:#fff;
        }
        .logo{
            padding: 10px 10px 5px 0px; 
            margin-right:15px; 
            border: 1px solid #000000;
        }
        .logoCW{
            padding: 0px; 
            margin-right: 0px; 
            border: 1px solid #000000;
        }
        .copy{
            vertical-align:central;
        }    

    </style>    
    <asp:ContentPlaceHolder runat="server" ID="HeadContent" />    
</head>
<body>
    <form runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="jquery-ui" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="colorbox" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>

            </Scripts>
        </asp:ScriptManager>

        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                    <a style="box-shadow: 0 2px 2px gainsboro;" runat="server" href="https://www.mhsglobal.com/"  target="_blank" >
                        <asp:Image ID="MHSlogo" runat="server" ImageUrl="~/Images/MHSLogoSmall.png" CssClass="logo navbar-brand" AlternateText="MHS Global's Main Site" /></a>
                    <a class="navbar-brand" style="box-shadow: 0 2px 2px gainsboro;" runat="server" href="~/Schedule">Manufacturing Scheduling Portal</a>
                    
                </div>
                <div class="navbar-collapse collapse">
                    <ul class="nav navbar-nav"></ul>
                     <asp:LoginView ID="LoginView1" runat="server">
                        <AnonymousTemplate>
                            <ul class="nav navbar-nav navbar-right">
                                <li><a style="box-shadow: 0 2px 2px gainsboro;" runat="server" href="~/Account/Login">Log in</a></li> 
                            </ul>
                        </AnonymousTemplate>
                        <LoggedInTemplate>
                            <div class="dropdown pull-right" style="padding-top:9px;">
                                <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
                                    Hello, <%: Context.User.Identity.GetUserName()  %>!
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a runat="server" href="~/Schedule.aspx">Schedule</a></li>
                                    <li class="divider"></li>
                                    <li><a runat="server" href="https://mhsh.sharepoint.com/sites/IQMSDev/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FIQMSDev%2FShared%20Documents%2FWork%20Instructions%2FSRS%20%2DIQMS%20Bolt%2DOn%20Work%20Instruction%2Epdf&parent=%2Fsites%2FIQMSDev%2FShared%20Documents%2FWork%20Instructions" target="_blank">Work Instructions</a></li>
                                    <li><a runat="server" href="https://mhsh.sharepoint.com/sites/IQMSDev/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FIQMSDev%2FShared%20Documents%2FTraining%20Material%2FTrainingDocumentation%2Epdf&parent=%2Fsites%2FIQMSDev%2FShared%20Documents%2FTraining%20Material" target="_blank">Training Document</a></li>
                                    <li class="divider"></li>
                                    <li><a runat="server" href="~/About.aspx">About</a></li>
                                    <li><a runat="server" href="~/Support.aspx">Support</a></li>
                                    <li class="divider"></li>
                                    <li><asp:LoginStatus runat="server" LogoutAction="Redirect" LogoutText="Log off" LogoutPageUrl="~/Default.aspx" OnLoggingOut="Unnamed_LoggingOut" /></li>
                                </ul>
                            </div>
                        </LoggedInTemplate>
                    </asp:LoginView>
               </div>
            </div>
        </div>
        <div class="container body-content">
            <asp:ContentPlaceHolder ID="MainContent" runat="server">
            </asp:ContentPlaceHolder>
            <hr />
            <footer>
                 <span class="copy">
                     <asp:Image ID="MHSlogoCW" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/MHSLogoSmall.png" CssClass=" logoCW" />
                     &copy; <%: DateTime.Now.Year %> - MHS Global Manufacturing Scheduling Portal
                 </span>            
            </footer>
        </div>
    </form>
</body>
</html>
