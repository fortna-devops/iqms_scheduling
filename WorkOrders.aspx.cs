﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

public partial class WorkOrders : System.Web.UI.Page
{
    //Define global Connection String
    string strConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

    protected void ds_Load(object sender, EventArgs e)
    {
        ((SqlDataSource)sender).Select(DataSourceSelectArguments.Empty);
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.QueryString.Keys.Count > 0)
        //{
        //	btnColorbox_Click(sender, e);
        //}
        Page.Validate();

        try
        {
            if (IsPostBack && IsValid)
            {

            }
            else
            {
                this.BindGrid();
            }

        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex.Message.ToString());
        }

    }

    private void BindGrid()
    {
        DataTable dt = new DataTable();
        SqlConnection con = new SqlConnection(strConnection);
        string strID = txtWo.Text;
        string Kanban = txtKanban.Text;
        String cell = Request["cell"];
        String date = Request["date"];

        if (strID == "")
        {
            strID = null;
        }
        if (Kanban == "")
        {
            Kanban = null;
        }

        if ((strID is null) && (Kanban is null))
        {
            SqlCommand com = new SqlCommand("[WO].[WORKORDER_select_byMFGCELL]", con);
            com.Parameters.AddWithValue("@cell", cell);
            com.Parameters.AddWithValue("@date", date);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            try
            {
                con.Open();
                da.Fill(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
        else
        {
            SqlCommand com = new SqlCommand("[WO].[WORKORDER_Search_byMFGCELL]", con);
            com.Parameters.AddWithValue("@cell", cell);
            com.Parameters.AddWithValue("@date", date);
            com.Parameters.AddWithValue("@KANBAN", Kanban);
            com.Parameters.AddWithValue("@ID", strID);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            try
            {
                con.Open();
                da.Fill(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }

        }
        gvWorkOrders.DataSource = dt;
        gvWorkOrders.DataBind();
        ViewState["dirState"] = dt;
        ViewState["sortdr"] = "Asc";
    }

    protected void Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtrslt = (DataTable)ViewState["dirState"];
        if (dtrslt.Rows.Count > 0)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "Desc")
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Asc";
                ViewState["sortdr"] = "Asc";
            }
            else
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Desc";
                ViewState["sortdr"] = "Desc";
            }
            gvWorkOrders.DataSource = dtrslt;
            gvWorkOrders.DataBind();


        }

    }

    protected void gvWorkOrder_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        switch (e.Row.RowType)
        {
            case DataControlRowType.Header:
                break;
            case DataControlRowType.DataRow:
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    HyperLink hl = (HyperLink)e.Row.FindControl("KANBAN");
                    if (hl != null)
                    {
                        DataRowView drv = (DataRowView)e.Row.DataItem;
                        string keyword = drv["KANBAN"].ToString().Trim();
                        hl.NavigateUrl = $"~/Kanban.aspx?Kanban=" + keyword;

                    }
                }

                break;
            default:
                break;
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        //Create sql connection and command
        SqlConnection con = new SqlConnection(strConnection);
        SqlCommand cmd = new SqlCommand();

        //Loop through gridview rows to find checkbox 
        //and check whether it is checked or not 
        for (int i = 0; i < gvWorkOrders.Rows.Count; i++)
        {
            CheckBox chkUpdate = (CheckBox)
               gvWorkOrders.Rows[i].Cells[0].FindControl("chkSelect");
            if (chkUpdate != null)
            {
                if (chkUpdate.Checked)
                {
                    // Get the values of textboxes using findControl
                    string strID = ((Label)gvWorkOrders.Rows[i].FindControl("lblID")).Text;
                    string strDate = ((TextBox)gvWorkOrders.Rows[i].FindControl("txtDate")).Text;
                    string strProdHrs = ((TextBox)gvWorkOrders.Rows[i].FindControl("txtProdHrs")).Text;
                    string strKanban = ((HyperLink)gvWorkOrders.Rows[i].FindControl("KANBAN")).Text;
                    string strSalesOrder = ((TextBox)gvWorkOrders.Rows[i].FindControl("txtSalesOrder")).Text;
                    string strJobNum = ((TextBox)gvWorkOrders.Rows[i].FindControl("txtJobNum")).Text;

                    using (cmd = new SqlCommand("[WO].[WORKORDER_update_StartDate]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@ID", SqlDbType.Float).Value = strID;
                        cmd.Parameters.Add("@PRODHRS", SqlDbType.Float).Value = strProdHrs;
                        cmd.Parameters.Add("@BUCKET_START_TIME", SqlDbType.NVarChar).Value = strDate;
                        cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = strKanban;
                        cmd.Parameters.Add("@SalesOrder", SqlDbType.NVarChar).Value = strSalesOrder;
                        cmd.Parameters.Add("@JobNum", SqlDbType.NVarChar).Value = strJobNum;

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }
        }
        this.BindGrid();
        UncheckAll();

    }

    protected void btnCascade_Click(object sender, EventArgs e)
    {

        //Create sql connection and command
        SqlConnection con = new SqlConnection(strConnection);
        SqlCommand cmd = new SqlCommand();

        //Loop through gridview rows to find checkbox 
        //and check whether it is checked or not 
        for (int i = 0; i < gvWorkOrders.Rows.Count; i++)
        {
            CheckBox chkUpdate = (CheckBox)
               gvWorkOrders.Rows[i].Cells[0].FindControl("chkSelect");
            if (chkUpdate != null)
            {
                if (chkUpdate.Checked)
                {
                    // Get the values of textboxes using findControl
                    string strID = ((Label)gvWorkOrders.Rows[i].FindControl("lblID")).Text;
                    string strDate = ((TextBox)gvWorkOrders.Rows[i].FindControl("txtDate")).Text;
                    string strKanban = ((HyperLink)gvWorkOrders.Rows[i].FindControl("KANBAN")).Text;

                    using (cmd = new SqlCommand("[WO].[WORKORDER_Cascade_Dates]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@ID", SqlDbType.Float).Value = strID;
                        cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = strKanban;
                        cmd.Parameters.Add("@BUCKET_START_TIME", SqlDbType.NVarChar).Value = strDate;

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }
        }
        this.BindGrid();
        UncheckAll();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        //Create sql connection and command
        SqlConnection con = new SqlConnection(strConnection);
        SqlCommand cmd = new SqlCommand();

        string strID = txtWo.Text;
        string Kanban = txtKanban.Text;
        String cell = Request["cell"];
        String date = Request["date"];

        if (strID == "")
        {
            strID = null;
        }
        if (Kanban == "")
        {
            Kanban = null;
        }

        if (strID is null && Kanban is null)
        {
            using (cmd = new SqlCommand("[WO].[WORKORDER_Select_byMFGCELL]", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@cell", SqlDbType.Text).Value = cell;
                cmd.Parameters.Add("@date", SqlDbType.Date).Value = date;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                this.BindGrid();
            }
        }
        else if(strID is null || Kanban is null)
        {
            using (cmd = new SqlCommand("[WO].[WORKORDER_Search_byMFGCELL]", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@cell", SqlDbType.Text).Value = cell;
                cmd.Parameters.Add("@date", SqlDbType.Date).Value = date;
                cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = Kanban;
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = strID;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                this.BindGrid();
            }
        }



    }

    private void UncheckAll()
    {
        foreach (GridViewRow row in gvWorkOrders.Rows)
        {
            CheckBox chkUncheck = (CheckBox)
                         row.FindControl("chkSelect");
            TextBox txtDate = (TextBox)row.FindControl("txtDate");
            TextBox txtProdHrs = (TextBox)row.FindControl("txtProdHrs");
            Label lblDate = (Label)row.FindControl("lblDate");
            Label lblProdHrs = (Label)row.FindControl("lblProdHrs");
            TextBox txtSalesOrder = (TextBox)row.FindControl("txtSalesOrder");
            TextBox txtJobNum = (TextBox)row.FindControl("txtJobNum");
            Label lblSalesOrder = (Label)row.FindControl("SalesOrder");
            Label lblJobNum = (Label)row.FindControl("JobNum");
            chkUncheck.Checked = false;
            lblDate.Visible = true;
            lblProdHrs.Visible = true;
            txtDate.Visible = false;
            txtProdHrs.Visible = false;
            lblSalesOrder.Visible = true;
            lblJobNum.Visible = true;
            txtSalesOrder.Visible = false;
            txtJobNum.Visible = false;
        }
    }

    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {

    }

    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkTest = (CheckBox)sender;
        GridViewRow grdRow = (GridViewRow)chkTest.NamingContainer;
        TextBox txtDate = (TextBox)grdRow.FindControl("txtDate");
        TextBox txtProdHrs = (TextBox)grdRow.FindControl("txtProdHrs");
        Label lblDate = (Label)grdRow.FindControl("lblDate");
        Label lblProdHrs = (Label)grdRow.FindControl("lblProdHrs");
        TextBox txtSalesOrder = (TextBox)grdRow.FindControl("txtSalesOrder");
        TextBox txtJobNum = (TextBox)grdRow.FindControl("txtJobNum");
        Label lblSalesOrder = (Label)grdRow.FindControl("SalesOrder");
        Label lblJobNum = (Label)grdRow.FindControl("JobNum");

        if (chkTest.Checked)
        {
            lblDate.Visible = false;
            lblProdHrs.Visible = false;
            txtDate.Visible = true;
            txtProdHrs.Visible = true;
            lblSalesOrder.Visible = false;
            lblJobNum.Visible = false;
            txtSalesOrder.Visible = true;
            txtJobNum.Visible = true;
        }
        else
        {
            lblDate.Visible = true;
            lblProdHrs.Visible = true;
            txtDate.Visible = false;
            txtProdHrs.Visible = false;
            lblSalesOrder.Visible = true;
            lblJobNum.Visible = true;
            txtSalesOrder.Visible = false;
            txtJobNum.Visible = false;
        }


    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void Update(object sender, EventArgs e)
    {

    }

    protected void gvWorkOrders_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //GridViewRow row = gvWorkOrders.Rows[e.RowIndex];	 
        //if (dsWorkOrders.UpdateParameters["ReturnValue"] != null) { dsWorkOrders.UpdateParameters.Remove(dsWorkOrders.UpdateParameters["ReturnValue"]); }
        //e.NewValues["ID"] = row.Cells[1].Text;
        //e.NewValues["PRODHRS"] = ((TextBox)row.Cells[3].Controls[1]).Text;
        //e.NewValues["START_TIME"] = ((TextBox)row.Cells[4].Controls[1]).Text;


        //dsWorkOrders.UpdateParameters["ID"].DefaultValue = row.Cells[1].Text;
        //dsWorkOrders.UpdateParameters["PRODHRS"].DefaultValue = ((TextBox)row.Cells[3].Controls[1]).Text;
        //dsWorkOrders.UpdateParameters["START_TIME"].DefaultValue = ((TextBox)row.Cells[4].Controls[1]).Text;
        //dsWorkOrders.Update();
        //var id = DataBinder.Eval(((GridView)sender).SelectedRow.DataItem, "Work Order");
        //var prdHrs = (TextBox)((GridView)sender).SelectedRow.FindControl("txtProdHrs");
        //var strtTime = (TextBox)((GridView)sender).SelectedRow.FindControl("txtDate");
        //e.NewValues["ID"] = id;
        //e.NewValues["PRODHRS"] = prdHrs.Text;
        //e.NewValues["START_TIME"] = strtTime.Text;
        //dsWorkOrders.UpdateParameters["PRODHRS"].DefaultValue = e.NewValues["PRODHRS"].ToString();
        //dsWorkOrders.UpdateParameters["START_TIME"].DefaultValue = e.NewValues["START_TIME"].ToString();
        //dsWorkOrders.Update();
    }
    protected void dsWorkOrders_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {

    }
    protected void gvWorkOrders_PreRender(object sender, EventArgs e)
    {
        var isDate = new DateTime();
        for (int i = 1; ((GridView)sender).HeaderRow != null && i < ((GridView)sender).HeaderRow.Cells.Count; i++)
            if (DateTime.TryParse(gvWorkOrders.HeaderRow.Cells[i].Text, out isDate)) { gvWorkOrders.HeaderRow.Cells[i].Text = gvWorkOrders.HeaderRow.Cells[i].Text.Substring(0, 6); }
    }

    protected void gvWorkOrders_DataBound(object sender, EventArgs e)
    {

    }

    protected void gvWorkOrders_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && ((GridView)sender).Caption == string.Empty)
        {
            ((GridView)sender).Caption = string.Format("{0} on {1:dddd, MMMM dd, yyyy}", DataBinder.Eval(e.Row.DataItem, "MFG CELL"), DataBinder.Eval(e.Row.DataItem, "Bucket_Start_Time"));
        }
    }
}