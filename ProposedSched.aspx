﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProposedSched.aspx.cs" Inherits="ProposedSched" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="gvWorkCenter" runat="server" AllowSorting="False"
                EmptyDataText="Error with loading datasource... " Width="90%" Caption=""
                BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="2px"
                CellPadding="4" ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" 
                OnRowDataBound="gvWorkCenter_RowDataBound">
                <AlternatingRowStyle BackColor="LightGray" />
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" CssClass="centerHeaderText"  />
                <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#F7F7DE" Height="30" />
                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                <SortedAscendingHeaderStyle BackColor="#848384" />
                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                <SortedDescendingHeaderStyle BackColor="#575357" />
            </asp:GridView>
        </div>
    </form>

</body>
</html>
