﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="About.aspx.cs" Inherits="About" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
        .centerHeaderText th {
            text-align: center;
        }

        .hide {
            visibility: hidden;
        }
        .my-class {
            padding-top: 30px;
            padding-bottom: 30px;
            margin-bottom: 30px;
            color: inherit;
            background-color: #eee;
            border-radius: 6px;
        }
    </style>
            <h2><b>About</b></h2>
            <div>
                <p class="container my-class">
                    <b><u>Property Of:</u></b> MHS Global<br />
                    <b><u>Version:</u></b> 1.1.0<br />
                    <b><u>Date:</u></b> 07/20/2019<br />
                    <b><u>Developers:</u></b> MHS Global, Meadon&Moore<br /><br /><br />
                    <b><u>Application Usage:</u></b> This application is used to schedule work orders down to the work center.<br />
                    Quantity of Production Hours can be changed based on work load. Production Hours Can be moved from day to day.<br />
                </p>
            </div>
            
                
            

</asp:Content>