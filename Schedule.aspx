﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Schedule.aspx.cs" Inherits="Schedule" EnableEventValidation="false" %>

<asp:Content ID="Content2" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <style>
        .centerHeaderText th {
            text-align: center;
        }

        .fakebutton {
            display: block;
            width: 115px;
            height: 40px;
            background: #A7A6A2;
            padding: 10px;
            text-align: center;
            border-radius: 5px;
            color: white;
            font-weight: bold;
            display:inline;
        }

        .hide {
            visibility: hidden;
        }
    </style>
        <h2><b>Schedule</b></h2>
            <div class="jumbotron">
                <div class="container-fluid" >
                    <div class="center-block" style="width: 35%;">
                        <label for="ddlCell" style="margin-right: 25px; position: relative;">MFG Type:<br />
                            <asp:DropDownList ID="ddlCell" runat="server" DataSourceID="dsCell" 
                                AppendDataBoundItems="true" DataTextField="MFG_TYPE">
                                <asp:ListItem Text="Select. . ." Value="Select" Selected="True"></asp:ListItem>
                            </asp:DropDownList>
                        </label>
                            <asp:SqlDataSource ID="dsCell" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>"
                                SelectCommand="BOM.STANDARD_select_MFG_TYPE" SelectCommandType="StoredProcedure" OnLoad="ds_Load"></asp:SqlDataSource>

                        <Label for="ddlFields" style="margin-right: 25px; position: relative; display: none;">Fields:<br />
                            <asp:DropDownList ID="ddlFields" runat="server">
                                <asp:ListItem Text="Utilization" Value="Percentages"></asp:ListItem>
                                <asp:ListItem Text="Hours" Value="hours"></asp:ListItem>
                            </asp:DropDownList>
                        </Label>
                            <asp:SqlDataSource ID="dsFields" runat="server" OnLoad="ds_Load"></asp:SqlDataSource>

                        <label for="txtDate" style="margin-right: 25px; position: relative;">From Date:<br /><input type="text" id="txtDate" class="txtDate" readonly="readonly" runat="server" style="height: 19px; width: 83px;" /></label>
                        <asp:RequiredFieldValidator ID="reqVal" runat="server" ErrorMessage="RequiredFieldValidator" ControlToValidate="txtDate" Display="None"></asp:RequiredFieldValidator>

                        <label for="ddlBuckets" style="margin-right: 25px; position: relative;">Days:<br />
                            <asp:DropDownList id="ddlBuckets" runat="server">
                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                <asp:ListItem Text="3" Value="3" Selected="True"></asp:ListItem>
                                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                            </asp:DropDownList>
                        </label>

                        <label for="ddlType" style="display:none">Type:<br />
                            <asp:DropDownList ID="ddlType" runat="server">
                                <asp:ListItem Text="Days" Value="Days"></asp:ListItem>
                                <asp:ListItem Text="Weeks" Value="Weeks"></asp:ListItem>
                            </asp:DropDownList>
                        </Label>

                        <label for="txtPerBucket" style="display:none">Per Bucket:<br /><input type="text" id="txtPerBucket" runat="server" /></label>

                        <asp:CheckBoxList ID="CheckBoxList1" runat="server" Visible="false" Enabled="False" >
                            <asp:ListItem Text="Show Past Load?" Value="past"></asp:ListItem>
                            <asp:ListItem Text="Show Future Load?" Value="future"></asp:ListItem>
                        </asp:CheckBoxList>
                        <br /><br />
                        <div class="center-block" style="margin: auto; width: 100%; text-align: center;">
                            <div id="dvButtons" style="visibility: visible;" >
                                <asp:Button id="aLoadData" class="button" runat="server" OnClick="btnLoadData_Click" OnClientClick="javascript:ShowProgressBar()" Text="Load Schedule . . ." Visible="true"/>
                            </div>
                             <div id="dvOR" style="visibility: visible; padding-top:25px" >
                                -OR-
                            </div>
                            <div id="dvKanbanSearch" style="visibility: visible; padding-top:25px" >
                                Kanban:
                                <asp:TextBox id="txtKanban" runat="server" onchange="UpdateLink();"></asp:TextBox>
                                <a id="mylink" class="fakebutton" href="/Kanban.aspx?Kanban=" target="_blank">Search</a>
                                
                            </div>
                            <div id="dvProgressBar" style="visibility: hidden; padding-top:15px;" >
                                <img src="/Scheduling/Images/MHSLogoSmall.gif" /><br />Loading Information.  Please Wait...
                            </div> 
                        </div>
                        
                   </div>
               </div>
            </div>
            <div style="text-align: center;">
                <div style="width: 90%; text-align: left; margin: auto;"> <!--string.Format("{0:dddd, MMMM dd, yyyy}", Eval("Bucket Start Date") ?? "no data") -->
                    <a id="aNewSched" runat="server" class="button open_ajax" visible="false">Create Adjusted Schedule . . .</a> 
                </div>
                <br />
               <asp:GridView ID="gvWorkCenter" runat="server" DataSourceID="dsWorkCenter" 
                    EmptyDataText="Data not loaded ... " Width="90%" Caption="" 
                    BackColor="White" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="2px"
                    CellPadding="4" ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" 
                    AllowSorting="True" OnRowDataBound="gvWorkCenter_RowDataBound" 
                    OnPreRender="gvWorkCenter_PreRender">
                    <AlternatingRowStyle BackColor="LightGray" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" CssClass="centerHeaderText"  />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                    <RowStyle BackColor="#F7F7DE" Height="30" />
                    <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#FBFBF2" />
                    <SortedAscendingHeaderStyle BackColor="#848384" />
                    <SortedDescendingCellStyle BackColor="#EAEAD3" />
                    <SortedDescendingHeaderStyle BackColor="#575357" />
                </asp:GridView>
                <asp:SqlDataSource ID="dsWorkCenter" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                    SelectCommand="dbo.Work_Center_select_byMFGtype" SelectCommandType="StoredProcedure" OnLoad="ds_Load" OnSelecting="dsWorkCenter_Selecting">
                    <SelectParameters>
                        <asp:ControlParameter Name="MFG_TYPE" ControlID="ddlCell" PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="ddlBuckets" Name="buckets" PropertyName="SelectedValue" Type="Int32" />
                        <asp:ControlParameter ControlID="txtDate" Type="String" Name="fromDate" PropertyName="Value" />
                        <asp:ControlParameter ControlID="ddlType" Name="type" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </div>
             <asp:HiddenField ID="hfDate" runat="server" ClientIDMode="Static" />
             <asp:HiddenField ID="hfCell" runat="server" ClientIDMode="Static" />
    <script src="Scripts/jquery-3.4.1.min.js"></script>
    <script src="jquery-ui-1.12.1.custom/jquery-ui.js"></script>
    <script src="Colorbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var $j = jQuery.noConflict();
            $j(document).ready(function () {
                $j(".txtDate").datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true });
                $j(".open_ajax").colorbox({ iframe: true, width: "100%", height: "100%", opacity: 40 });

                var prm = Sys.WebForms.PageRequestManager.getInstance();
                prm.add_endRequest(function () {
                    $j(".txtDate").datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true });
                    $j(".txtDate").on('focus', function () {
                        $j.datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true });
                    });
                    $j(".open_ajax").colorbox({ iframe: true, width: "100%", height: "100%", opacity: 40 });
                    $j(".open_ajax").on('click', function (e) {
                        $j.colorbox({ iframe: true, width: "100%", height: "100%", opacity: 40 });
                    });

                });

                $j("[id$='aNewSched']").on("click", function (e) {
                    $j.colorbox();
                });

            });

            $j(".open_ajax").colorbox({ iframe: true, width: "80%", height: "90%", opacity: 40 });

            //var prm = Sys.WebForms.PageRequestManager.getInstance();
            //prm.add_endRequest(function() {
            //    $(".txtDate").datepicker();
            //    $(".txtDate").on('focus', function () {
            //        $.datepicker({
            //            maxViewMode: 2,
            //            todayBtn: true,
            //            clearBtn: true
            //        });
            //    });
            //});

            $j("[id$='aNewSched']").on("click", function (e) {
                $j.colorbox();
            });

        });

        $(document).on('cbox_open', function () {
            $('body').css({ overflow: 'scroll' });
        }).on('cbox_closed', function () {
            document.getElementById('dvButtons').style.visibility = 'hidden';
            document.getElementById('dvProgressBar').style.visibility = 'visible';
            location.reload(false);
            $('body').css({ overflow: 'scroll' });
            });

        function ShowProgressBar() {
            document.getElementById('dvButtons').style.visibility = 'hidden';
            document.getElementById('dvProgressBar').style.visibility = 'visible';
        }

        function HideProgressBar() {
            document.getElementById('dvButtons').style.visibility = 'visible';
            document.getElementById('dvProgressBar').style.visibility = "hidden";
        }

        function UpdateLink()
        {
            var Kanban = document.getElementById("<%=txtKanban.ClientID%>").value;
            document.getElementById("mylink").href = "Kanban.aspx?Kanban=" + Kanban;
        }

        function UnLoadWindow() {
            document.getElementById('dvProgressBar').style.visibility = 'visible';
            document.getElementById("<%=txtKanban.ClientID%>").value = '';
        }

        window.onbeforeunload = UnLoadWindow;


        

    </script>
</asp:Content>

