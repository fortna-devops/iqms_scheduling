﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace MHSglobalScheduling
{
	public static class LinqExtensions
	{
		public static DataTable ToPivotTable<T, TColumn, TRow, TData>(
			this IEnumerable<T> source,
			Func<T, TColumn> columnSelector,
			Expression<Func<T, TRow>>  rowSelector,
			Func<IEnumerable<T>, TData> dataSelector)
		{
			DataTable table = new DataTable();
			var rowName = GetCorrectPropertyName(() => rowSelector.Name);//((MemberExpression)rowSelector.Body).Member.Name;
			table.Columns.Add(new DataColumn(rowName));
			var columns = source.Select(columnSelector).Distinct();

			foreach (var column in columns)
				table.Columns.Add(new DataColumn(column.ToString()));

			var rows = source.GroupBy(rowSelector.Compile())
							 .Select(rowGroup => new
							 {
								 Key = rowGroup.Key,
								 Values = columns.GroupJoin(
									 rowGroup,
									 c => c,
									 r => columnSelector(r),
									 (c, columnGroup) => dataSelector(columnGroup))
							 });

			foreach (var row in rows)
			{
				var dataRow = table.NewRow();
				var items = row.Values.Cast<object>().ToList();
				items.Insert(0, row.Key);
				dataRow.ItemArray = items.ToArray();
				table.Rows.Add(dataRow);
			}

			return table;
		}
		public static string GetCorrectPropertyName<TEntity>(Expression<Func<TEntity>> expression)
		{
			if (expression.Body is MemberExpression)
			{
				return ((MemberExpression)expression.Body).Member.Name;
			}
			else
			{
				var op = ((UnaryExpression)expression.Body).Operand;
				return ((MemberExpression)op).Member.Name;
			}
		}
		public static Dictionary<TFirstKey, Dictionary<TSecondKey, TValue>> Pivot<TSource, TFirstKey, TSecondKey, TValue>(this IEnumerable<TSource> source, Func<TSource, TFirstKey> firstKeySelector, Func<TSource, TSecondKey> secondKeySelector, Func<IEnumerable<TSource>, TValue> aggregate)
		{
			var retVal = new Dictionary<TFirstKey, Dictionary<TSecondKey, TValue>>();

			var l = source.ToLookup(firstKeySelector);
			foreach (var item in l)
			{
				var dict = new Dictionary<TSecondKey, TValue>();
				retVal.Add(item.Key, dict);
				var subdict = item.ToLookup(secondKeySelector);
				foreach (var subitem in subdict)
				{
					dict.Add(subitem.Key, aggregate(subitem));
				}
			}

			return retVal;
		}

	}
}