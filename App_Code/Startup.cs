﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MHSglobalScheduling.Startup))]
namespace MHSglobalScheduling
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
