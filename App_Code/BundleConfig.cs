﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.UI;

namespace MHSglobalScheduling
{
    public class BundleConfig
    {
        // For more information on Bundling, visit https://go.microsoft.com/fwlink/?LinkID=303951
        public static void RegisterBundles(BundleCollection bundles)
        {
			bundles.UseCdn = true;

			ScriptManager.ScriptResourceMapping.AddDefinition("jquery-ui", new ScriptResourceDefinition()
																{ Path = "~/jquery-ui-1.12.1.custom/jquery-ui.min.js",
																  DebugPath = "~/jquery-ui-1.12.1.custom/jquery-ui.js",
																  CdnPath = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js",
																  CdnDebugPath = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js",
																  CdnSupportsSecureConnection = true});

			ScriptManager.ScriptResourceMapping.AddDefinition("colorbox", new ScriptResourceDefinition()
																{ Path = "~/Colorbox/colorbox-min.js", 
																  DebugPath = "~/Colorbox/colorbox.js",
																  CdnPath = "https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox-min.js",
																  CdnDebugPath = "https://cdnjs.cloudflare.com/ajax/libs/jquery.colorbox/1.6.4/jquery.colorbox.js",
																  CdnSupportsSecureConnection = true});
																																					
			bundles.Add(new ScriptBundle("~/bundles/WebFormsJs").Include(
                            "~/Scripts/WebForms/WebForms.js",
                            "~/Scripts/WebForms/WebUIValidation.js",
                            "~/Scripts/WebForms/MenuStandards.js",
                            "~/Scripts/WebForms/Focus.js",
                            "~/Scripts/WebForms/GridView.js",
                            "~/Scripts/WebForms/DetailsView.js",
                            "~/Scripts/WebForms/TreeView.js",
                            "~/Scripts/WebForms/WebParts.js"));

            // Order is very important for these files to work, they have explicit dependencies
            bundles.Add(new ScriptBundle("~/bundles/MsAjaxJs").Include(
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjax.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxApplicationServices.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxTimer.js",
                    "~/Scripts/WebForms/MsAjax/MicrosoftAjaxWebForms.js"));


			// Use the Development version of Modernizr to develop with and learn from. Then, when you’re
			// ready for production, use the build tool at https://modernizr.com to pick only the tests you need
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                            "~/Scripts/modernizr-*"));

			bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
							"~/Scripts/jquery-ui-{version}.js",
							"~/Scripts/jquery-ui-{version}.min.js"));

			bundles.Add(new ScriptBundle("~/bundles/colorbox").Include(
							"~/Colorbox/jquery.colorbox.js",
							"~/Colorbox/jquery.colobox-min.js"));

     }
    }
}