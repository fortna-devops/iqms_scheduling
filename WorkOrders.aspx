﻿﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="~/WorkOrders.aspx.cs" Inherits="WorkOrders" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.Common" %>
<%@ Import Namespace="System.Web" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <asp:PlaceHolder ID="PlaceHolder1" runat="server">
        <%: Scripts.Render("~/bundles/modernizr") %>
    </asp:PlaceHolder>

    <style>
        .centerHeaderText th {
            text-align: center;
        }
    </style>

    <webopt:bundlereference runat="server" path="~/Content/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager runat="server">
            <Scripts>
                <%--To learn more about bundling scripts in ScriptManager see https://go.microsoft.com/fwlink/?LinkID=301884 --%>
                <%--Framework Scripts--%>
                <asp:ScriptReference Name="MsAjaxBundle" />
                <asp:ScriptReference Name="jquery" />
                <asp:ScriptReference Name="jquery-ui" />
                <asp:ScriptReference Name="bootstrap" />
                <asp:ScriptReference Name="colorbox" />
                <asp:ScriptReference Name="WebForms.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebForms.js" />
                <asp:ScriptReference Name="WebUIValidation.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebUIValidation.js" />
                <asp:ScriptReference Name="MenuStandards.js" Assembly="System.Web" Path="~/Scripts/WebForms/MenuStandards.js" />
                <asp:ScriptReference Name="GridView.js" Assembly="System.Web" Path="~/Scripts/WebForms/GridView.js" />
                <asp:ScriptReference Name="DetailsView.js" Assembly="System.Web" Path="~/Scripts/WebForms/DetailsView.js" />
                <asp:ScriptReference Name="TreeView.js" Assembly="System.Web" Path="~/Scripts/WebForms/TreeView.js" />
                <asp:ScriptReference Name="WebParts.js" Assembly="System.Web" Path="~/Scripts/WebForms/WebParts.js" />
                <asp:ScriptReference Name="Focus.js" Assembly="System.Web" Path="~/Scripts/WebForms/Focus.js" />
                <asp:ScriptReference Name="WebFormsBundle" />
                <%--Site Scripts--%>

            </Scripts>
        </asp:ScriptManager>
        <div style="width: 100%; text-align: left; margin-left:75px">
            Search Work Order#:
            <asp:TextBox style="width: 50%; text-align: left; padding-right:25px;" ID="txtWo" runat="server"></asp:TextBox>
            Search Kanban#:
            <asp:TextBox style="width: 50%; text-align: left" ID="txtKanban" runat="server"></asp:TextBox>
            <asp:Button class="button" ID="btnSearch" OnClick="btnSearch_Click" runat="server" Text="Search" OnClientClick="javascript:ShowSearchBar()"/>
            <img id="dvSearchBar" style="visibility:hidden; padding-left:10px;" src="/Scheduling/Images/MHSLogoSmall.gif" />
        </div> 
        <div style="width: 100%; text-align: center">  
            <asp:GridView ID="gvWorkOrders" runat="server" AllowPaging="True" AllowSorting="true" onsorting="Sorting"
                EmptyDataText="Data not loaded ... " Width="90%" 
                BackColor="White" PageSize="100" BorderColor="#DEDFDE" BorderStyle="Solid" BorderWidth="2px" AutoGenerateEditButton="False"
                CellPadding="4" ForeColor="Black" GridLines="Vertical" HorizontalAlign="Center" AutoGenerateColumns="False" 
                OnRowUpdating="gvWorkOrders_RowUpdating" OnRowCreated="gvWorkOrders_RowCreated" OnRowDataBound="gvWorkOrder_RowDataBound" OnDataBound="gvWorkOrders_DataBound" OnPreRender="gvWorkOrders_PreRender" >
                <AlternatingRowStyle BackColor="LightGray" />
                
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Work Order"  SortExpression="ID">
                        <ItemTemplate>
                            <asp:Label ID="lblID" Text='<%# Bind("ID") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="KANBAN" SortExpression="KANBAN">
                        <ItemTemplate>
                            <asp:HyperLink ID="KANBAN" Text='<%# Bind("KANBAN") %>' runat="server" Target="_blank" SortExpression="KANBAN"></asp:HyperLink>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="MFGNO" SortExpression="MFGNO">
                        <ItemTemplate>
                            <asp:Label ID="lblMFGNO" Text='<%# Eval("MFGNO") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ItemNo" SortExpression="ItemNo">
                        <ItemTemplate>
                            <asp:Label ID="lblItemNo" Text='<%# Eval("ItemNo") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity">
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" Text='<%# Eval("Quantity") %>' runat="server"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                            <asp:TemplateField HeaderText="Job Number" SortExpression="JobNum">
                        <ItemTemplate>
                            <asp:Label ID="JobNum" Text='<%# Eval("JobNum") %>' runat="server"></asp:Label>
                            <asp:TextBox ID="txtJobNum" Text='<%# Bind("JobNum") %>' runat="server" Visible="false"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sales Order" SortExpression="SalesOrder">
                        <ItemTemplate>
                            <asp:Label ID="SalesOrder" Text='<%# Eval("SalesOrder") %>' runat="server"></asp:Label>
                            <asp:TextBox ID="txtSalesOrder" Text='<%# Bind("SalesOrder") %>' runat="server" Visible="false"></asp:TextBox>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="WorkOrder Prod Hrs" SortExpression="ProdHrs">
                        <ItemTemplate>
                            <asp:Label ID="lblProdHrs" Text='<%# Eval("ProdHrs", "{0:0.00}") %>' runat="server"></asp:Label>
                            <asp:TextBox ID="txtProdHrs" Text='<%# Bind("ProdHrs", "{0:0.00}") %>' runat="server" Visible="false"></asp:TextBox>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtProdHrs" Text='<%# Bind("ProdHrs", "{0:0.00}") %>' runat="server"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Bucket Start Date" SortExpression="Bucket Start Date">
                        <ItemTemplate>
                            <asp:Label ID="lblDate" Text='<%# Eval("Bucket_Start_Time", "{0:dddd, MMMM dd, yyyy}") %>' runat="server"></asp:Label>
                            <asp:TextBox ID="txtDate" CssClass="txtDate" runat="server" Text='<%# Bind("Bucket_Start_Time", "{0:dd-MMM-yy}") %>' Visible="false"></asp:TextBox>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDate" CssClass="txtDate" runat="server" Text='<%# Bind("Bucket_Start_Time", "{0:dd-MMM-yy}") %>'></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#CCCC99" />
                <HeaderStyle CssClass="centerHeaderText" BackColor="#6B696B" Font-Bold="True" ForeColor="White"/>
                <pagersettings mode="NumericFirstLast" position="Bottom" pagebuttoncount="5" PreviousPageText="Prev" NextPageText="Next"/>
                <PagerStyle font-size="Large" Width="10"  BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
                <RowStyle BackColor="#F7F7DE" Height="30" />
                <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#FBFBF2" />
                <SortedAscendingHeaderStyle BackColor="#848384" />
                <SortedDescendingCellStyle BackColor="#EAEAD3" />
                <SortedDescendingHeaderStyle BackColor="#575357" />
            </asp:GridView>
            <table id="emptyTable" runat="server" visible="false">
                <tr>
                    <td colspan="4">
                        No Records Found!
                    </td>
                </tr>
            </table>

            <div id="dvButtons" style="visibility: visible;" >
                <asp:Button Style="margin-top:10px; margin-right:10px" ID="btnUpdate" runat="server" OnClick="btnUpdate_Click" OnClientClick="javascript:ShowProgressBar()" Text="Update" Visible="true"/>
                <asp:Button Style="margin-top:10px; margin-right:10px" ID="btnCascade" runat="server" OnClick="btnCascade_Click" OnClientClick="javascript:ShowProgressBar()" Text="Cascade Date" Visible="true"/><br />
            </div>
            
            <div id="dvProgressBar" style="visibility: hidden;" >
                <img src="/Scheduling/Images/MHSLogoSmall.gif" /><br />Updating Information.  Please Wait...
            </div>
            <asp:SqlDataSource ID="dsWorkOrders" runat="server" OnLoad="ds_Load"
                ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" 
                SelectCommand="WO.WORKORDER_select_byMFGCELL" SelectCommandType="StoredProcedure"
                FilterExpression="ID = '{0}' OR KANBAN = '{1}'">
                <%--UpdateCommand="WO.WORKORDER_update_StartDate" UpdateCommandType="StoredProcedure"--%>
                
                <SelectParameters>
                    <asp:QueryStringParameter Name="cell" QueryStringField="cell" Type="String" />
                    <asp:QueryStringParameter DbType="Date" Name="date" QueryStringField="date" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ID" Type="Double" />
                    <asp:Parameter Name="PRODHRS" Type="Double" />
                    <asp:Parameter Name="BUCKET_START_TIME" Type="String" />
                    <asp:Parameter Name="KANBAN" Type="Double" />
                    <asp:Parameter Name="SalesOrder" Type="String" />
                    <asp:Parameter Name="JobNum" Type="String" />
                </UpdateParameters>
                <FilterParameters>
                    <asp:ControlParameter Name="ID" ControlID="txtWo" PropertyName="Text" />
                    <asp:ControlParameter Name="KANBAN" ControlID="txtKanban" PropertyName="Text" />
                </FilterParameters>
            </asp:SqlDataSource>
        </div>
    <script type="text/javascript">
        //var $jr = jQuery.noConflict();
        $(document).ready(function () {
            var myDate = new Date($('.txtDate').val());
            $(".txtDate").datepicker({ changeMonth: true, changeYear: true, showButtonPanel: true });
            $(".txtDate").datepicker('setDate', myDate);
        });

        function ShowProgressBar() {
            document.getElementById('dvButtons').style.visibility = 'hidden';
            document.getElementById('dvProgressBar').style.visibility = 'visible';
        }

        function HideProgressBar() {
            document.getElementById('dvButtons').style.visibility = 'visible';
            document.getElementById('dvProgressBar').style.visibility = "hidden";
        }

        function ShowSearchBar() {
            document.getElementById('dvSearchBar').style.visibility = 'visible';
            
        }

        function HideSearchBar() {
            document.getElementById('dvSearchBar').style.visibility = 'hidden';
        }

        $(document).on('focus', ".txtDate", function () {
            $(this).datepicker({
                changeMonth: true, changeYear: true, showButtonPanel: true});
            //$jr(this).datepicker('setDate', Date.parse($jr(this).val()) ? $jr(this).val() : new Date());
        });

    </script>
    </form>
</body>
</html>
