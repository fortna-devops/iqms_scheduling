﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;

public partial class Kanban : System.Web.UI.Page
{
    //Define global Connection String
    string strConnection = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

    protected void ds_Load(object sender, EventArgs e)
    {
        ((SqlDataSource)sender).Select(DataSourceSelectArguments.Empty); 
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.QueryString.Keys.Count > 0)
        //{
        //	btnColorbox_Click(sender, e);
        //}
        Page.Validate();

        try
        {
            if (IsPostBack && IsValid)
            {

            }
            else
            {
                this.BindGrid();
            }

        }
        catch (Exception ex)
        {
            Debug.WriteLine(ex.Message.ToString());
        }

    }

    private void BindGrid()
    {
        DataTable dt = new DataTable();
        SqlConnection con = new SqlConnection(strConnection);
        String Kanban = Request["KANBAN"];
        string strID = txtSearch.Text;
        if (strID == "")
        {
            strID = null;
        }

        if (strID is null)
        {
            SqlCommand com = new SqlCommand("[WO].[WORKORDER_select_byKANBAN]", con);
            com.Parameters.AddWithValue("@KANBAN", Kanban);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            try
            {
                con.Open();
                da.Fill(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }
        }
        else
        {
            SqlCommand com = new SqlCommand("[WO].[WORKORDER_Search_byKANBAN]", con);
            com.Parameters.AddWithValue("@KANBAN", Kanban);
            com.Parameters.AddWithValue("@ID", strID);
            com.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(com);
            try
            {
                con.Open();
                da.Fill(dt);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con.State == ConnectionState.Open)
                    con.Close();
            }

        }
        gvKanbans.DataSource = dt;
        gvKanbans.DataBind();
        ViewState["dirState"] = dt;
        ViewState["sortdr"] = "Asc";
    }

    protected void Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable dtrslt = (DataTable)ViewState["dirState"];
        if (dtrslt.Rows.Count > 0)
        {
            if (Convert.ToString(ViewState["sortdr"]) == "Desc")
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Asc";
                ViewState["sortdr"] = "Asc";
            }
            else
            {
                dtrslt.DefaultView.Sort = e.SortExpression + " Desc";
                ViewState["sortdr"] = "Desc";
            }
            gvKanbans.DataSource = dtrslt;
            gvKanbans.DataBind();


        }

    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {

        //Create sql connection and command
        SqlConnection con = new SqlConnection(strConnection);
        SqlCommand cmd = new SqlCommand();

        //Loop through gridview rows to find checkbox 
        //and check whether it is checked or not 
        for (int i = 0; i < gvKanbans.Rows.Count; i++)
        {
            CheckBox chkUpdate = (CheckBox)
               gvKanbans.Rows[i].Cells[0].FindControl("chkSelect");
            if (chkUpdate != null)
            {
                if (chkUpdate.Checked)
                {
                    // Get the values of textboxes using findControl
                    string strID = ((Label)gvKanbans.Rows[i].FindControl("lblID")).Text;
                    string strDate = ((TextBox)gvKanbans.Rows[i].FindControl("txtDate")).Text;
                    string strProdHrs = ((TextBox)gvKanbans.Rows[i].FindControl("txtProdHrs")).Text;
                    string strKanban = ((Label)gvKanbans.Rows[i].FindControl("KANBAN")).Text;
                    string strSalesOrder = ((TextBox)gvKanbans.Rows[i].FindControl("txtSalesOrder")).Text;
                    string strJobNum = ((TextBox)gvKanbans.Rows[i].FindControl("txtJobNum")).Text;

                    using (cmd = new SqlCommand("[WO].[WORKORDER_update_StartDate]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@ID", SqlDbType.Float).Value = strID;
                        cmd.Parameters.Add("@PRODHRS", SqlDbType.Float).Value = strProdHrs;
                        cmd.Parameters.Add("@BUCKET_START_TIME", SqlDbType.NVarChar).Value = strDate;
                        cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = strKanban;
                        cmd.Parameters.Add("@SalesOrder", SqlDbType.NVarChar).Value = strSalesOrder;
                        cmd.Parameters.Add("@JobNum", SqlDbType.NVarChar).Value = strJobNum;

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }
        }
        this.BindGrid();
        UncheckAll();

    }

    protected void btnCascade_Click(object sender, EventArgs e)
    {

        //Create sql connection and command
        SqlConnection con = new SqlConnection(strConnection);
        SqlCommand cmd = new SqlCommand();

        //Loop through gridview rows to find checkbox 
        //and check whether it is checked or not 
        for (int i = 0; i < gvKanbans.Rows.Count; i++)
        {
            CheckBox chkUpdate = (CheckBox)
               gvKanbans.Rows[i].Cells[0].FindControl("chkSelect");
            if (chkUpdate != null)
            {
                if (chkUpdate.Checked)
                {
                    // Get the values of textboxes using findControl
                    string strID = ((Label)gvKanbans.Rows[i].FindControl("lblID")).Text;
                    string strDate = ((TextBox)gvKanbans.Rows[i].FindControl("txtDate")).Text;
                    string strKanban = ((Label)gvKanbans.Rows[i].FindControl("KANBAN")).Text;

                    using (cmd = new SqlCommand("[WO].[WORKORDER_Cascade_Dates]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add("@ID", SqlDbType.Float).Value = strID;
                        cmd.Parameters.Add("@BUCKET_START_TIME", SqlDbType.NVarChar).Value = strDate;
                        cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = strKanban;

                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }

                }
            }
        }
        this.BindGrid();
        UncheckAll();

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        //Create sql connection and command
        SqlConnection con = new SqlConnection(strConnection);
        SqlCommand cmd = new SqlCommand();

        string strID = txtSearch.Text;
        string strKanban = Request["KANBAN"];
        string strKanban2 = txtSearch.Text;

        if (strID == "")
        {
            strID = null;
        }

        if (strID is null)
        {
            using (cmd = new SqlCommand("[WO].[WORKORDER_Select_byKANBAN]", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = strKanban;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                this.BindGrid();
            }
        }
        else
        {
            using (cmd = new SqlCommand("[WO].[WORKORDER_Search_byKANBAN]", con))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = strID;
                cmd.Parameters.Add("@KANBAN", SqlDbType.Float).Value = strKanban;

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                this.BindGrid();
            }
        }

        

    }

    private void UncheckAll()
    {
        foreach (GridViewRow row in gvKanbans.Rows)
        {
            CheckBox chkUncheck = (CheckBox)
                         row.FindControl("chkSelect");
            TextBox txtDate = (TextBox)row.FindControl("txtDate");
            TextBox txtProdHrs = (TextBox)row.FindControl("txtProdHrs");
            Label lblDate = (Label)row.FindControl("lblDate");
            Label lblProdHrs = (Label)row.FindControl("lblProdHrs");
            TextBox txtSalesOrder = (TextBox)row.FindControl("txtSalesOrder");
            TextBox txtJobNum = (TextBox)row.FindControl("txtJobNum");
            Label lblSalesOrder = (Label)row.FindControl("SalesOrder");
            Label lblJobNum = (Label)row.FindControl("JobNum");
            chkUncheck.Checked = false;
            lblDate.Visible = true;
            lblProdHrs.Visible = true;
            txtDate.Visible = false;
            txtProdHrs.Visible = false;
            lblSalesOrder.Visible = true;
            lblJobNum.Visible = true;
            txtSalesOrder.Visible = false;
            txtJobNum.Visible = false;
        }
    }

    protected void OnPaging(object sender, GridViewPageEventArgs e)
    {

    }

    protected void chkSelect_CheckedChanged(object sender, EventArgs e)
    {
        CheckBox chkTest = (CheckBox)sender;
        GridViewRow grdRow = (GridViewRow)chkTest.NamingContainer;
        TextBox txtDate = (TextBox)grdRow.FindControl("txtDate");
        TextBox txtProdHrs = (TextBox)grdRow.FindControl("txtProdHrs");
        Label lblDate = (Label)grdRow.FindControl("lblDate");
        Label lblProdHrs = (Label)grdRow.FindControl("lblProdHrs");
        TextBox txtSalesOrder = (TextBox)grdRow.FindControl("txtSalesOrder");
        TextBox txtJobNum = (TextBox)grdRow.FindControl("txtJobNum");
        Label lblSalesOrder = (Label)grdRow.FindControl("SalesOrder");
        Label lblJobNum = (Label)grdRow.FindControl("JobNum");

        if (chkTest.Checked)
        {
            lblDate.Visible = false;
            lblProdHrs.Visible = false;
            txtDate.Visible = true;
            txtProdHrs.Visible = true;
            lblSalesOrder.Visible = false;
            lblJobNum.Visible = false;
            txtSalesOrder.Visible = true;
            txtJobNum.Visible = true;
        }
        else
        {
            lblDate.Visible = true;
            lblProdHrs.Visible = true;
            txtDate.Visible = false;
            txtProdHrs.Visible = false;
            lblSalesOrder.Visible = true;
            lblJobNum.Visible = true;
            txtSalesOrder.Visible = false;
            txtJobNum.Visible = false;
        }


    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void Update(object sender, EventArgs e)
    {

    }

    protected void gvKanbans_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        //GridViewRow row = gvKanbans.Rows[e.RowIndex];	 
        //if (dsKanbans.UpdateParameters["ReturnValue"] != null) { dsKanbans.UpdateParameters.Remove(dsKanbans.UpdateParameters["ReturnValue"]); }
        //e.NewValues["ID"] = row.Cells[1].Text;
        //e.NewValues["PRODHRS"] = ((TextBox)row.Cells[3].Controls[1]).Text;
        //e.NewValues["START_TIME"] = ((TextBox)row.Cells[4].Controls[1]).Text;


        //dsKanbans.UpdateParameters["ID"].DefaultValue = row.Cells[1].Text;
        //dsKanbans.UpdateParameters["PRODHRS"].DefaultValue = ((TextBox)row.Cells[3].Controls[1]).Text;
        //dsKanbans.UpdateParameters["START_TIME"].DefaultValue = ((TextBox)row.Cells[4].Controls[1]).Text;
        //dsKanbans.Update();
        //var id = DataBinder.Eval(((GridView)sender).SelectedRow.DataItem, "Work Order");
        //var prdHrs = (TextBox)((GridView)sender).SelectedRow.FindControl("txtProdHrs");
        //var strtTime = (TextBox)((GridView)sender).SelectedRow.FindControl("txtDate");
        //e.NewValues["ID"] = id;
        //e.NewValues["PRODHRS"] = prdHrs.Text;
        //e.NewValues["START_TIME"] = strtTime.Text;
        //dsKanbans.UpdateParameters["PRODHRS"].DefaultValue = e.NewValues["PRODHRS"].ToString();
        //dsKanbans.UpdateParameters["START_TIME"].DefaultValue = e.NewValues["START_TIME"].ToString();
        dsKanbans.Update();
    }
    protected void dsKanbans_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        //var prdHrs = (TextBox)gvKanbans.Rows[gvKanbans.SelectedIndex].FindControl("txtProdHrs");
        //var stTime = (TextBox)gvKanbans.Rows[gvKanbans.SelectedIndex].FindControl("txtStartTime");
        //e.Command.Parameters["@ID"].Value = DataBinder.Eval(gvKanbans.SelectedRow.DataItem, "Work Order");
        //e.Command.Parameters["@PRODHRS"].Value = prdHrs.Text;
        //e.Command.Parameters["@START_TIME"].Value = stTime.Text;

    }
    protected void gvKanbans_PreRender(object sender, EventArgs e)
    {
        var isDate = new DateTime();
        for (int i = 1; ((GridView)sender).HeaderRow != null && i < ((GridView)sender).HeaderRow.Cells.Count; i++)
            if (DateTime.TryParse(gvKanbans.HeaderRow.Cells[i].Text, out isDate)) { gvKanbans.HeaderRow.Cells[i].Text = gvKanbans.HeaderRow.Cells[i].Text.Substring(0, 6); }
    }

    protected void gvKanbans_DataBound(object sender, EventArgs e)
    {

    }

    protected void gvKanbans_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && ((GridView)sender).Caption == string.Empty)
        {
            ((GridView)sender).Caption = "Detail for Kanban #: " + string.Format("{0}", DataBinder.Eval(e.Row.DataItem, "KANBAN"));
        }
    }
}