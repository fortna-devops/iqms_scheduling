﻿<%@ Application Language="C#" %>
<%@ Import Namespace="MHSglobalScheduling" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e)
    {
        RouteConfig.RegisterRoutes(RouteTable.Routes);
        BundleConfig.RegisterBundles(BundleTable.Bundles);
    }

    void Session_Start(object sender, EventArgs e)
    {
        if (HttpContext.Current.User.Identity.IsAuthenticated) {  IdentityHelper.RedirectToReturnUrl("~/Schedule.aspx", Response);  }
    }

</script>
